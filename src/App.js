import React, { Component } from 'react';
import { Switch, Route, /*Redirect*/ } from 'react-router-dom'
//import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import OKRList from './components/OKRList';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheckSquare, faCoffee , faCaretDown, faEdit, faTrash} from '@fortawesome/free-solid-svg-icons'
import AddObjective from './components/AddObjective';
import AddKeyResult from './components/AddKeyResult';
import gql from "graphql-tag";

//import { InMemoryCache } from 'apollo-cache-inmemory';
//import { Router, Route, Link, IndexRoute, hashHistory, browserHistory } from 'react-router';

library.add(faCheckSquare, faCoffee, faCaretDown, faTrash, faEdit)

const ADD_OBJECTIVE = gql`
  mutation AddObjective($description: String, $year: Int, $quarter: Int, $missionId: Int) {
    createObjective(input: { objective: { description: $description, year: $year, quarter: $quarter, grade: 0, missionId: $missionId}}
    ){
      objective {
        id
        description
        year
        quarter
        grade
        missionId
      }
    }
  }
`

const o_add_variables = {description: null, year: null, quarter: null, missionId: null}

const UPDATE_OBJECTIVE = gql`
  mutation updateObj($description: String, $year: Int, $quarter: Int, $missionId: Int, $id: Int, $grade: Float) {
    updateoObjectiveById(input: {id: $id, objectivePatch: { description: $description, year: $year, quarter: $quarter, grade: $grade, missionId: $missionId}}
    ){
      objective {
        id
        description
        year
        quarter
        grade
        missionId
      }
    }
  }
`

const o_update_variables = {description: null, year: null, quarter: null, missionId: null, id: null, grade: null}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      yearDataFromHeader: null,
      quarterDataFromHeader: null,
    };
    this.variables = {}
  }

  conditionalVariables() {
    if((this.state.yearDataFromHeader > 0) && (this.state.quarterDataFromHeader > 0)) {
      this.variables = {year: this.state.yearDataFromHeader, quarter:this.state.quarterDataFromHeader}
      this.setState(this.state)
    }
    else if (this.state.quarterDataFromHeader > 0) {
      this.variables = {quarter:this.state.quarterDataFromHeader}
      this.setState(this.state)
    }
    else if (this.state.yearDataFromHeader > 0) {
      this.variables = {year: this.state.yearDataFromHeader}
      this.setState(this.state)
    }
    else {
      this.variables = {}
      this.setState(this.state)
    }
    return
  }

  yearCallback = (yearFromHeader) => {
    this.setState({ yearDataFromHeader: yearFromHeader},() => this.conditionalVariables())
  }
  quarterCallback = (quarterFromHeader) => {
    this.setState({ quarterDataFromHeader: quarterFromHeader},() => this.conditionalVariables())
  }

  render() {
    return(
      <div>
        <Header yearCallbackFromParent={this.yearCallback} quarterCallbackFromParent={this.quarterCallback}/>
        <div>
          <Switch>
            <Route exact path="/okrs" render={() => <OKRList variables={this.variables}/>}/>
            <Route exact path="/addobjective" render={() => <AddObjective mutation={ADD_OBJECTIVE} type="add" variables={o_add_variables} />}/>
            <Route exact path="/updateobjective" render={() => <AddObjective mutation={UPDATE_OBJECTIVE} type="update" variables={o_update_variables} />} />
            <Route exact path="/addkeyresult" render={() => <AddKeyResult />}/>
            <Route exact path="/updatekeyresult" render={() => <AddKeyResult />} />
          </Switch>
        </div>
      </div>
    )
  }
}

export default App;
