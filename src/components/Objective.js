import React, { Component } from 'react'
import KeyResultList from './KeyResultList'

class Objective extends Component {
  constructor(props) {
    super(props)
    this.state = {grade: this.props.objective.grade * 100}
  }

  getSum(total, num) {
    return total + num
  }

  objectiveCallback = () => {
    var sumArray = this.props.objective.child.nodes.map(grades => grades.grade)
    var average = (sumArray.reduce(this.getSum) / sumArray.length) * 100
    this.setState({grade: Math.round(average)})
  }

  render() {
    return (
      <div style={{marginLeft: "20px"}}>
        <div style={{paddingTop: "20px"}}>
          <h3 style={{float:"left"}}>O{this.props.objective.id}: {this.props.objective.description}</h3>
          <h3 className="grade">{this.state.grade}%</h3>
          <div style={{clear:"left"}}></div>
          <hr/>
        </div>
        <KeyResultList key={this.props.objective.child.nodes.id} keyResult={this.props.objective.child.nodes} objectiveGrade={this.props.objective.grade} callbackFromParent={this.objectiveCallback}></KeyResultList>
      </div>
    )
  }
}

export default Objective