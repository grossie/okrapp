import React, { Component } from 'react';
//import ObjectiveList from './ObjectiveList';
//import gql from "graphql-tag";
import { Mutation } from "react-apollo"
import { Button, Form, /*FormGroup, Label, Input, FormText */} from 'reactstrap';


/*const DELETE_OBJECTIVE = gql`
  mutation deleteObj($id: Int) {
    deleteObjectiveById(input: {id: $id}) {
      deletedObjectiveId
    }
  }
    
`

const GET_MISSIONS = gql`
{
  allMissions(orderBy: ID_ASC) {
    nodes {
      id
      description
    }
  }
}
`*/

class AddObjective extends Component {
  constructor(props) {
    super(props)
    this.state = { mutation: this.props.mutation, variables: this.props.variables, type: this.props.type }
  }

  render() {
    return (
      <Mutation mutation={this.state.mutation} variables={this.state.variables}>
        {() => (
          <div>
            <Form onSubmit={this.handleSubmit}>
              <label>
                Name:
                <input type="text" />
              </label>
            </Form>
            <Button color="primary" type="submit">Submit</Button>
          </div>
        )}
      </Mutation>
    )
  }
}

export default AddObjective