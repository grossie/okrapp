import React, { Component } from 'react'
import KeyResult from './KeyResult'
//import { Query } from 'react-apollo'
//import gql from 'graphql-tag'


class KeyResultList extends Component {  
  
  middleCallback = () => {
    this.props.callbackFromParent();
  }

  render() {
    return (
      <div>
        {this.props.keyResult.map(keyResult => 
          <div key={keyResult.id}>
            <KeyResult keyResult={keyResult} callbackFromParent={this.middleCallback}></KeyResult>
          </div>
        )}
      </div>
    )
  }
}

/*const KR_QUERY = gql`
  query filterKeyResults($objectivesId: Int) {
  allKeyResults (condition: {objectiveId: $objectivesId}) {
    nodes {
      id
      description
      objectiveId
      grade
      startDate
      dueDate
      krMeasurable
      notes
    }
  }
}
  
`
query filterKeyResults($objectiveId: ID)
  {
    objective(id: $objectiveId) {
      keyResultsByObjectiveId(orderBy:ID_ASC){
        nodes {
          id
          description
          objectiveId
          grade
          startDate
          dueDate
          krMeasurable
          notes
        }
      }
    }
  }*/
  
  /*render() {
    return (
      <div><h1>Objective ID {this.props.objectiveIdFromParent}</h1>
      <Query query={KR_QUERY} variables={{ $objectivesId:this.props.objectiveIdFromParent }}>
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching</div>
          if (error) return <div>Error</div>
          const keyResultsToRender = data.allKeyResults.nodes
          return (
            <div>
              {keyResultsToRender.map(keyResult => 
                <KeyResult key={keyResult.id} keyResult={keyResult}/>
              )}
            </div>
          )
        }}
        </Query>
        </div>
    )
  }
}
*/
export default KeyResultList