import React, { Component } from 'react'
import OKR from './OKR'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

/*const OKR_QUERY = gql`
  {
    allMissions {
      nodes {
        id
        description
        child: objectivesByMissionId (orderBy: DESCRIPTION_ASC) {
          nodes {
            id
            description
            quarter
            grade
            child: keyResultsByObjectiveId (orderBy:DESCRIPTION_ASC) {
              nodes {
                id
                description
                grade
                startDate
                dueDate
                krMeasurable
                notes
              }
            }
          }
        } 
      }
    }
  }
`
const MISSION_QUERY = gql`
  {
    allMissions {
      nodes {
        id
        description
      }
    }
  }
`

class OKRList extends Component {

  list(data) {
    const children = (nodes) => {
      if(nodes) {
        return <p>{ this.list(nodes) }</p>
      }
    }
    return data.map((node, index) => {
      console.log(node);
      return <OKR key={ node.props.okr.id } description={ node.props.okr.description }>
        { children(node.nodes) }
        </OKR>
    })
  }
  render() {
    return (
      <div>
        <Query query={OKR_QUERY}>
          {({ loading, error, data }) => {
            if (loading) return <div>Fetching</div>
            if (error) return <div>Error</div>
      
            const OKRsToRender = data.allMissions.nodes
            console.log(OKRsToRender);

            return (
              <div>
                {this.list(OKRsToRender.map(okr => <OKR key={okr.id} okr={okr}/>))}
              </div>
            )
          }}
          </Query>
        </div>
    )
  }
}

export default OKRList

 <Query query={Mission_QUERY}>
{({ loading, error, data }) => {
if (loading) return <div>Fetching</div>
if (error) return <div>Error</div>

const missionsToRender = data.allMissions.nodes
return (
  <div>
    {missionsToRender.map(mission => <Mission key={mission.id} mission={mission}/>)}
  </div>
)
}}
</Query>*/