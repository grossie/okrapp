import React, { Component } from 'react'
import Objective from './Objective'
//import { Query } from 'react-apollo'
//import gql from 'graphql-tag'
//import KeyResultList from './KeyResultList';

class ObjectiveList extends Component {
  render() {
    return (
      <div>
        {this.props.objective.map(objective => 
          <div key={objective.id}>
            <Objective objective={objective}></Objective>
          </div>
        )}
      </div>
    )
  }
}
  /*render() {
    return (
      <div>
      <Query query={OBJECTIVE_QUERY} variables={{ missionId:this.props.missionIdFromParent }}>
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching</div>
          if (error) return <div>Error</div>
          console.log(data)
          const objectivesToRender = data.allObjectives.nodes
          console.log(this.props.missionIdFromParent)

          
          return (
            <div>
              {objectivesToRender.map(objective => 
              <div key={objective.id}>
                <Objective key={objective.id} objective={objective}/>
                <KeyResultList objectiveIdFromParent={objective.id}/>
              </div>
              )}
            </div>
          )
        }}
        </Query>
        </div>
    )
  }
}*/
/*const OBJECTIVE_QUERY = gql`
  query filteredObjectives($missionId: Int) {
      allObjectives (condition: {missionId: $missionId}) {
        nodes {
          id
          description
          year
          quarter
          missionId
          grade
        }
      }
    }
  
`
query filteredObjectives($missionId: ID) {
    mission (id: $missionId) {
        objectivesByMissionId(orderBy: ID_ASC) {
        nodes {
          id
          description
          year
          quarter
          missionId
          grade
        }
      }
    }
  }*/
export default ObjectiveList