import React, { Component } from 'react';
import Moment from 'react-moment';
import { Collapse, Button } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Slider from 'rc-slider'
import 'rc-slider/assets/index.css';


class KeyResult extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: false, value: this.props.keyResult.grade * 100 };
    this.trackStyle = ""
    this.handleStyle = ""
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  saveGrade = () => {
    this.props.keyResult.grade = Math.round(this.state.value) / 100
    this.props.callbackFromParent()
  }

  handleSliderChange = (value) => {
    this.setState({value,})
  }

  render() {
    if (this.state.value < 50) {
      this.trackStyle = {backgroundColor: "#B3000C"}
      this.handleStyle = {border: "solid 2px #B3000C"}
    }
    else if (this.state.value > 69) {
      this.trackStyle = {backgroundColor: "#00B32C"}
      this.handleStyle = {border: "solid 2px #00B32C"}
    }
    else {
      this.trackStyle = {backgroundColor: "#F3BC2E"}
      this.handleStyle = {border: "solid 2px #F3BC2E"}
    }
    return (
      <div style={{marginTop: "10px", overflow:"auto"}}>
        <h5 style={{float:"left", margin:"0 auto"}}>KR{this.props.keyResult.id}: {this.props.keyResult.description}</h5>
        <h5 className="grade">{this.state.value}%</h5>
        <div style={{paddingTop:"30px", width:"97%", margin:"0 auto"}}>
          <Slider defaultValue={this.state.value} onChange={this.handleSliderChange} onAfterChange={this.saveGrade} trackStyle={this.trackStyle} handleStyle={this.handleStyle} />
        </div>
        <div style={{clear:"left"}}></div>
        <Button color="link" onClick={this.toggle} style={{ float:"right" }}><FontAwesomeIcon icon="caret-down"></FontAwesomeIcon></Button>
        <Collapse isOpen={this.state.collapse} style={{ width: "95%", float:"left"}}>
            <table cellPadding="20">
              <tbody>
                <tr>
                  <td><b>Start Date:</b> <Moment format="MM/DD/YYYY">{this.props.keyResult.startDate}</Moment></td>
                  <td><b>Due Date:</b> <Moment format="MM/DD/YYYY">{this.props.keyResult.dueDate}</Moment></td>
                  <td><b>KR Measurable:</b> {this.props.keyResult.krMeasurable}</td>
                  <td><b>Notes:</b> {this.props.keyResult.notes}</td>
                  <td><Button color="secondary"><FontAwesomeIcon icon="edit"></FontAwesomeIcon></Button></td>
                </tr>
              </tbody>
            </table>
          </Collapse>
          <div style={{clear:"right"}}></div>
      </div>
    )
  }
}

export default KeyResult

/*<Button color="danger" style={{marginLeft:"5px"}}><FontAwesomeIcon icon="trash"></FontAwesomeIcon></Button>*/