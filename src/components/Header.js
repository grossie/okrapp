import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'



const YEAR_QUERY = gql`
  {
    allObjectives {
      nodes {
        year
      }
    }
  }
`

class Header extends Component {
 
    constructor(props) {
        super(props);
        this.state = {year: "", quarter: ""};

        this.selectedQuarter = "";
        this.selectedYear = ""; 

        this.handleYearOnChange = this.handleYearOnChange.bind(this);
        this.handleQuarterOnChange = this.handleQuarterOnChange.bind(this);

    }

    handleYearOnChange(e) {
        this.setState({year: e.target.value});
        this.props.yearCallbackFromParent(e.target.value);
    
      }
    
    handleQuarterOnChange(e) {
        this.setState({quarter: e.target.value});
        this.props.quarterCallbackFromParent(e.target.value);
    }

    render() {
        return (
            <div>
                <header>
                    <div className="topnav">
                        <Link to="/okrs" className="">
                            OKRs
                        </Link>
                        <Link to="/addobjective" className="">
                            Add Objective
                        </Link>
                        <Link to="/addkeyresult" className="">
                            Add Key Result
                        </Link>
                        <div style={{float:"right", padding:"0"}}>
                            <Query query={YEAR_QUERY}>
                                {({ loading, error, data }) => {
                                if (loading) return <div>Fetching</div>
                                if (error) return <div>Error</div>
                            
                                const yearsToRender = data.allObjectives.nodes
                                let uniqueYears = [...new Set(yearsToRender.map(objective => objective.year))]
                                return (
                                    <div>
                                        <div style={{padding:"0", paddingRight:"10px"}}>Year: </div>
                                        <select ref={this.state.year} onChange={this.handleYearOnChange}>
                                            <option value=''>All</option>
                                            {uniqueYears.map(year => <option key={year} value={year}>{year}</option>)}
                                        </select>
                                    </div>
                                )
                                }}
                            </Query>
                            <div>
                                <div style={{padding:"0", paddingRight:"10px"}}>Quarter:   </div>
                                <select value={this.state.quarter} onChange={this.handleQuarterOnChange}>
                                    <option value=''>All</option>
                                    <option value="1">Q1</option>
                                    <option value="2">Q2</option>
                                    <option value="3">Q3</option>
                                    <option value="4">Q4</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        )
    }
}

export default withRouter(Header)