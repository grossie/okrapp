import React, { Component } from 'react'
import ObjectiveList from './ObjectiveList';

class Mission extends Component {
  render() {
    return (
      <div>
        <h2 style={{textDecoration:"underline", fontWeight: "bold"}}>M{this.props.mission.id}: {this.props.mission.description}</h2>
        <ObjectiveList key={this.props.mission.child.nodes.id} objective={this.props.mission.child.nodes}></ObjectiveList>
      </div>
    )
  }
}

export default Mission