import React, { Component } from 'react'
import Mission from './Mission'
//import ObjectiveList from './ObjectiveList'
//import { Query } from 'react-apollo'
//import gql from 'graphql-tag'



class MissionList extends Component {
  render() {
    return (
      <div key={this.props.mission.id}>
        <Mission mission={this.props.mission}></Mission>
      </div>
    )
  }
}


  /*
  const NODE_QUERY = gql`
  {
    allMissions {
      nodes {
        id
        description
      }
    }
  }
`
  constructor(props) {
    super(props);
    this.state = {
      missionIdFromMission: null,
    };
  }

  missionIdCallback = (missionId) => {
    this.setState({ missionIdFromMission: missionId})
  }
  
  render() {
    return (
      <Query query={NODE_QUERY}>
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching</div>
          if (error) return <div>Error</div>

          const missionsToRender = data.allMissions.nodes;
          return (
            <div>
              {missionsToRender.map(mission =>
              <div key={mission.id}>
                <Mission key={mission.id} mission={mission}/>
                <ObjectiveList missionIdFromParent={mission.id}/>
              </div>
              )}
            </div>
          )
        }}
        </Query>
    )
  }
}*/

export default MissionList