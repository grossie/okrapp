import React, { Component } from 'react'
import MissionList from './MissionList'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import { Jumbotron } from 'reactstrap'

const OKR_QUERY = gql`
  query okrQuery($year: Int, $quarter: Int)
    {
      allMissions {
        nodes {
          id
          description
          child: objectivesByMissionId (orderBy:ID_ASC, condition: {year: $year, quarter: $quarter}) {
            nodes {
              id
              description
              quarter
              year
              grade
              child: keyResultsByObjectiveId (orderBy:ID_ASC) {
                nodes {
                  id
                  description
                  grade
                  startDate
                  dueDate
                  krMeasurable
                  notes
                  objectiveId
                }
              }
            }
          } 
        }
      }
    }
`


class OKRList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      missionIdForObjective: null,
      objectiveIdForKR: null,
      year: this.props.year,
      quarter: this.props.quarter
    };
  }

  render() {
    return (
      <Query query={OKR_QUERY} variables={this.props.variables}>
        {({ loading, error, data }) => {
          if (loading) return <div></div>
          if (error) return <div>Error</div>

          const missionsToRender = data.allMissions.nodes;
          return (
            <div>
              {missionsToRender.map(mission =>
              <div key={mission.id} style={{paddingTop:"20px"}}>
                <Jumbotron style={{width:"90%", margin:"auto", paddingTop:"20px"}}>
                  <MissionList key={mission.id} mission={mission}/>
                </Jumbotron>
              </div>
              )}
            </div>
          )
        }}
      </Query>
    )
  }
}

export default OKRList