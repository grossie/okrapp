FROM node:8.12

WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY . .
RUN npm run build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.15.5
COPY --from=0 /app/build/ /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY --from=0 /app/nginx.conf /etc/nginx/conf.d/default.conf